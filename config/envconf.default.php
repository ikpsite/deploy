<?php

/* Environment-specific configuration defaults */

$ikpDevelop = false;
$ikpReadOnly = false;
$ikpReadOnlyMode = "maintenance";

# For a development environment only
# $ikpDevDomain = "";

<?php

# Protect against web entry
if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

$ikpConfDir = __DIR__;

require ("$ikpConfDir/InitSettings.php");
require ("$ikpConfDir/secret/PrivateSettings.php");
require ("$ikpConfDir/SiteSettings.php");

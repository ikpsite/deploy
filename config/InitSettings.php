<?php

/* Initialize Settings */

$ikpPath = dirname($ikpConfDir);

$ikpReadOnlyMode = "maintenance";

$ikpReadOnlyMsg = [
	"upgrade" => "Upgrading MediaWiki",
	"maintenance" => "Iron Kingdom Pages is currently on read-only mode for maintenace purposes",
	"backups" => "Site read-only to allow database backup"
];

require ("$ikpConfDir/envconf.default.php");
require ("$ikpConfDir/envconf.php");

$ikpDomain = !($ikpDevelop) ? "tharram.net" : $ikpDevDomain;

$ikpProc = $ikpDevelop ? "http" : "https";

if ($ikpReadOnly){
	$wgReadOnly = ( PHP_SAPI === 'cli' ) ? false : $ikpReadOnlyMsg[$ikpReadOnlyMode];
}

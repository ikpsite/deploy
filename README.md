# IKP Deployment

Deployment repository for Iron Kingdom Pages

For Iron Kingdom Pages use only. If you wish to set up your own MediaWiki instance download the latest stable version from the [official website](https://www.mediawiki.org).

## Directories

- `app/` - Contains MediaWiki installation. Files there are gitignored.
- `assets/` - Stores asset files for Iron Kingdom Pages
- `images/` - Images uploaded to Iron Kingdom Pages. Files there are gitignored.
- `docroot/` - Document root for Iron Kingdom pages
- `config/` - Configuration settings
- `tools/` - Tools to help manage Iron Kingdom Pages
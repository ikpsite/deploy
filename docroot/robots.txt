# Robots.txt for Iron Kingdom Pages

# General rules

User-agent: *
Allow: /w/load.php?
Disallow: /w/
Disallow: /wiki/::
Disallow: /wiki/Special:

# Allow the Internet Archiver to index action=raw and thereby store the raw wikitext of pages
User-agent: ia_archiver
Allow: /w/index.php?*&action=raw

# Disallow certain bots
# Some may respect robots.txt, and some may not.

User-agent: MJ12bot
Disallow: /

User-agent: AhrefsBot
Disallow: /

User-agent: DotBot
Disallow: /

<?php

http_response_code(404);
header("Content-Type: text/html; charset=utf-8");
$uri = urldecode($_SERVER['HTTP_X_IKP_URI']);
$e_uri = htmlspecialchars($uri);
$suggest_uri = "/wiki$e_uri";


$output = <<<HERE
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>404: Page not found</title>
        <link rel="shortcut icon" href="/assets/logo/favicon.ico">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href="/assets/css/error.css?102918" />
    </head>
    <body>
        <div id="main">
            <h1>Page not found</h1>
            <p>The page you are looking for was not found</p>
            <p>Request URL: <b>$e_uri</b></p>
            <p>You may have wanted to go to: <b><a href="$suggest_uri">$suggest_uri</a></b></p>
        </div>
    </body>
</html>
HERE;

echo $output;

#!/bin/bash

# This script must be run from the tools directory.

ikp_toolsdir=$PWD

cd ../app/extensions

for extname in `cat $ikp_toolsdir/extensions.txt`; do
  if [ ! -d "$extname" ]; then
    echo "Downloading $extname..."
    git clone https://gerrit.wikimedia.org/r/p/mediawiki/extensions/$extname --branch REL1_$1
  else
    echo "$extname already downloaded"
  fi
done

#!/bin/bash

# This script must be run from the tools directory

cd ../app/skins

git clone https://gerrit.wikimedia.org/r/p/mediawiki/skins/MinervaNeue --branch REL1_$1

git clone https://gitlab.com/ikpsite/Agarul.git

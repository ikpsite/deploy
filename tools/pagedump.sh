#!/bin/bash

# This script must be run from the tools directory

ikp_toolsdir=$PWD

ikp_workdir="$(dirname "$PWD")"

ikp_tstamp=$(date +%Y%m%d_%H%M%S)

cd ../app/maintenance

php dumpBackup.php --full > ${ikp_workdir}/backup/dump/dump_${ikp_tstamp}.xml

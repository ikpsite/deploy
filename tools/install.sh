#!/bin/bash

# This script must be run from the tools directory

ikp_toolsdir=$PWD

ikp_workdir="$(dirname "$PWD")"

ikp_mwver_major="1.$1"

ikp_mwver_minor=$2

ikp_mwver="${ikp_mwver_major}.${ikp_mwver_minor}"

cd ../tmp

wget https://releases.wikimedia.org/mediawiki/${ikp_mwver_major}/mediawiki-${ikp_mwver}.tar.gz

tar -xzf mediawiki-${ikp_mwver}.tar.gz

mv mediawiki-${ikp_mwver} ../app

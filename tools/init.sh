#!/bin/bash

# This script must be run from the tools directory

ikp_toolsdir=$PWD

ikp_workdir="$(dirname "$PWD")"

ln -s ../LocalSettings.php ${ikp_workdir}/app/LocalSettings.php

ln -s ../composer.local.json ${ikp_workdir}/app/composer.local.json
